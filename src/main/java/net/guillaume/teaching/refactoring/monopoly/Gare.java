package net.guillaume.teaching.refactoring.monopoly;

public class Gare extends Propriete {

	public Gare(String nom, int coutAchat, int loyer) {
		super(nom, coutAchat, loyer);
	}

	@Override
	public int calculerLoyer() {
		Joueur proprietaire = getProprietaire();
		return getLoyerInitial() * (int) Math.pow(2, proprietaire.getProprietesGare()-1);
	}

	@Override
	public void augmenterProprietes(Joueur joueurPayeur) {
		joueurPayeur.augmenterProprietaiesGare();
	}
	

}
