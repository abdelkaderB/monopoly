package net.guillaume.teaching.refactoring.monopoly;

public abstract class Case {

	private final String nom;

	public Case(String nom) {
		this.nom = nom;
	}
	public String getNom() {
		return nom;
	}
	public abstract void appliquerRegles(Joueur joueurPayeur);

}
