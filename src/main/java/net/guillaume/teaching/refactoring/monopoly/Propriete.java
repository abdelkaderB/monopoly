package net.guillaume.teaching.refactoring.monopoly;

abstract class Propriete extends Case {
	private final int coutAchat;
	private final int loyerInitial;
	private final boolean libre;
	private Joueur Proprietaire;
	public Propriete(String nom, int coutAchat, int loyerInitial) {
		super(nom);
		this.coutAchat = coutAchat;
		this.loyerInitial = loyerInitial;
		this.libre = true;
	}

	public int getCoutAchat() {
		return coutAchat;
	}

	public int getLoyerInitial() {
		return loyerInitial;
	}

	public boolean isLibre() {
		return libre;
	}

	public Joueur getProprietaire() {
		return Proprietaire;
	}

	public void setProprietaire(Joueur proprietaire) {
		Proprietaire = proprietaire;
	}

	public void payerLoyer(Joueur joueurPayeur) {
		Joueur proprietaire=getProprietaire();
		int prixLoyer=calculerLoyer();
		proprietaire.incrementerMontant(prixLoyer);
		joueurPayeur.decrementerMontant(prixLoyer);
	}
	
	public void acheter(Joueur joueurPayeur) {
		if(coutAchat<=joueurPayeur.getMontant()){	
			joueurPayeur.decrementerMontant(coutAchat);
			setProprietaire(joueurPayeur);
			augmenterProprietes(joueurPayeur);
		}
		if(joueurPayeur.getNom().equals("Ambre"))
		System.out.println(getNom()+"/ "+joueurPayeur.getMontant()+"/ "+joueurPayeur.getNom()+", coutAchat :"+coutAchat);
	}
	
	public void appliquerRegles(Joueur joueurPayeur){
		
		if(isLibre())
			acheter(joueurPayeur);
		else
			payerLoyer(joueurPayeur);
		
		
		
	}
	
	public abstract int calculerLoyer();
	public abstract void augmenterProprietes(Joueur joueurPayeur);

}
