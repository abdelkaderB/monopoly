package net.guillaume.teaching.refactoring.monopoly;

public enum Couleur {
	MARRON(2), VIOLET(3), BLEU(3), ORANGE(3), ROUGE(3), JAUNE(3), VERT(3), BLEUMARINE(2);
	private final int nombreTerrain ;

	private Couleur(int nombreTerrain ) {
		this.nombreTerrain  = nombreTerrain ;
	}

	public int getNombreTerrain () {
		return nombreTerrain ;
	}

}
