package net.guillaume.teaching.refactoring.monopoly;

public class Speciale extends Case {

	public Speciale(String nom) {
		super(nom);
	} 

	@Override
	public void appliquerRegles(Joueur joueurPayeur) {
		if ("Impot sur le Revenu".equals(this.getNom())) {  // si il arrive sur impot
			int prixPayer=((int) Math.floor(joueurPayeur.getMontant() * 0.9)-joueurPayeur.getMontant());
			joueurPayeur.incrementerMontant(prixPayer);
        }
        if ("Taxe de Luxe".equals(this.getNom())) { // si il arrive sur taxe de luxe
            joueurPayeur.decrementerMontant(5 * joueurPayeur.getTotalDes());
        }
        if ("Aller en Prison".equals(this.getNom())) { // si il arrive sur aller en prison
        	joueurPayeur.allerPrison();
        }
		
	}

}
