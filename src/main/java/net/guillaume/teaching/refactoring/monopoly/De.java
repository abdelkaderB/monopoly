package net.guillaume.teaching.refactoring.monopoly;

public class De {

	private int valeurFace; // valeur du dé
	private static final int NOMBRE_FACES = 6; // nombre de faces

	public De() {
		valeurFace = 1; // initialise la valeur du dé à 1
	}

	public int getValeur() { // renvoie la valeur du dé.
		return valeurFace;
	}

	public void rouler () { // génère une nouvelle valeur aléatoire comprise
							// entre 1 et 6.
		valeurFace = (int) (Math.random() * NOMBRE_FACES) + 1;
	}

}