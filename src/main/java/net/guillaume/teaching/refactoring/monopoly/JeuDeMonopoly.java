package net.guillaume.teaching.refactoring.monopoly;

import java.util.ArrayList;
import java.util.Collections;

public class JeuDeMonopoly {

    private final ArrayList<Joueur> joueurs = new ArrayList<>();
//    private final CoupleDes combinaison;
    private boolean stop = false;
    private ArrayList<Constructible> caseLibreAAchat = new ArrayList<>();
    private Plateau plateau ;
    private int nombreDouble=0;



    public JeuDeMonopoly() {
        plateau= Plateau.getInstance();
        joueurs.add(new Joueur("Marina","Elle"));
        joueurs.add(new Joueur("Ambre", "Elle"));
        joueurs.add(new Joueur("Loubna","Elle"));
        joueurs.add(new Joueur("Mathieu","Il"));
        joueurs.add(new Joueur("Cedric","Il"));
//        combinaison = new CoupleDes();
        // caseLibreAAchat=  new ArrayList<>(plateau.getCaseAchetable());
    }


    public void jouerUnePartie() {
        while (!stop) {
            for (Joueur joueur : joueurs) {
            	nombreDouble=0;
                jouerUnTour(joueur);
            }
        }
        afficheFinDePartie();
    }


    private void jouerUnTour(Joueur unjoueur) {
        //On joue un tour
        if (!stop) { 
        	unjoueur.lancer();
    // SI DOUBLE
            if (unjoueur.aFaitUnDouble()) {
                if (!unjoueur.estAuPrison()) {
                	nombreDouble++;
                	if(nombreDouble>=3){
                		unjoueur.allerPrison();
                		System.out.println(unjoueur.getNom() + " est jete en prison.");
                	}
                	else {
                		unjoueur.deplacer();
                		System.out.println(unjoueur.getNom() +" rejoue.");
                		jouerUnTour(unjoueur);
                	}
                }
                if (unjoueur.estAuPrison()) { 
                    unjoueur.deplacer();
                    //unjoueur.ouSuisJe();
                    if (unjoueur.getMontant() < 0)  System.out.println(unjoueur.getNom() + " a perdu. " + unjoueur.getSexe() + " n'a plus d'argent !!!!");
                }
            }
    // SI PAS DOUBLE
            else {
                if (!unjoueur.estAuPrison()) {
                    unjoueur.deplacer();
                }
               // unjoueur.ouSuisJe();
                if (unjoueur.getMontant() < 0) System.out.println(unjoueur.getNom() + " a perdu. " + unjoueur.getSexe() + " n'a plus d'argent !!!!");
            }
        }
    }
    private void afficheFinDePartie() {
        System.out.println("La partie est terminee !!!");   // L'affichage de la suite etait pas deamnde mais ca parait plus coherent
        trie();
         System.out.println("Le vainqueur est " + joueurs.get(0).getNom() + " avec " + joueurs.get(0).getMontant() +".");
        joueurs.get(0).afficherLesProprietes() ;
        for (int i=1; joueurs.size()>i; i++) {
            if(joueurs.get(i).getMontant()>0){
            System.out.println(joueurs.get(i).getNom() + " est " + (i + 1) + " place avec " + joueurs.get(i).getMontant() +".");
            joueurs.get(i).afficherLesProprietes();
        }
            else {
                System.out.println(joueurs.get(i).getNom() + " est " + (i + 1) + " place avec 0 argent.");
                joueurs.get(i).afficherLesProprietes();
            }
        }
    }

    private void trie() {  // non demande mais il me parait logique d afficher dans ordre
        Collections.sort(joueurs);
        Collections.sort(joueurs, Collections.reverseOrder());
    }




}

