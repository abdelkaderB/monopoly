package net.guillaume.teaching.refactoring.monopoly;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Joueur implements Comparable {

	private static final int MONTANT_INITIAL = 400;
	private static final int MAXIMUM_TOURS = 100;
	private Plateau plateau;
    private final CoupleDes coupleDes;
    private final String nom;
    private final String sexe;
    private int montant;
    private Case position;
    private int tour;  // indique le nombr de tour de plateau
    private boolean rejouer; // indique si le joueur va rejouer suite a une double
    private boolean liberable;
    Map<Couleur,Integer> proprietes=new HashMap<>();    // indique le nombre de propriété possede de telle ou telle couleur

    
    private int proprietesGare;
    
    private int indexCaseCourante;
    
    private ArrayList<Constructible> casespossedes;    // contient la liste des propriété possedes

    public Joueur(String nom, String sexe) {

    	plateau= Plateau.getInstance();
        position = plateau.getAt(Plateau.DEPART);
        this.nom = nom;
        this.sexe= sexe;
        coupleDes = new CoupleDes();

        montant = MONTANT_INITIAL;
        tour = 0;
        rejouer = false;
        liberable = false;
        proprietes.put(Couleur.MARRON, 0);
        proprietes.put(Couleur.BLEU, 0);
        proprietes.put(Couleur.VIOLET, 0);
        proprietes.put(Couleur.ORANGE, 0);
        proprietes.put(Couleur.ROUGE, 0);
        proprietes.put(Couleur.JAUNE, 0);
        proprietes.put(Couleur.VERT, 0);
        proprietes.put(Couleur.BLEUMARINE, 0);
        proprietesGare=0;
        
        indexCaseCourante=0;
        casespossedes= new ArrayList<Constructible>();
    }

    public boolean getLiberable() {
        return liberable;
    }

	public int getMontant() {
        return montant;
    }

    public String getNom() {
        return nom;
    }

    public String getSexe() {
        return sexe;
    }
    
    public int getProprietesGare() {
		return proprietesGare;
	}
	
	public int getTotalDes(){
		return coupleDes.faitLaSomme();
	}

    public boolean rejoue() {
        return rejouer;
    }

    public boolean estFinDePartie() { // condition de fin de partie
        return tour == MAXIMUM_TOURS || montant < 0;
    }
    

	public boolean aTousProprietesMemeCouleur(Couleur couleur){  // renvoye le nombre de propriete d un certain couleur
	   return couleur.getNombreTerrain()==proprietes.get(couleur);
	}
	public void augmenterProprietaiesCouleur(Couleur couleur){
		proprietes.put(couleur, proprietes.get(couleur)+1);
	}
	public void decrementerMontant(int somme ){
		if(montant>=somme)
			montant-=somme;
	}
	public void incrementerMontant(int somme ){
		montant+=somme;
	}
	
	public void augmenterProprietaiesGare(){
		proprietesGare++;
	}
	
	public boolean estAuPrison(){
		Case prison=plateau.getAt(Plateau.PRISON);
		return prison.getNom().equals(position.getNom()); 
	}
	
	public boolean aFaitUnDouble(){ return coupleDes.estUnDouble();}
	
	public void allerPrison(){
		deplacerVersindex(Plateau.PRISON);
	}
	private void deplacerVersindex(int newIndex){
		position=plateau.getAt(newIndex);
	}

//    // apres chaque tour soit il sort soit on incremente son nombre de tour
//    public void liberationEnVue() {
//        tourPrison++;
//        System.out.println( nom + " passe 1 tour en  prison.");
//        allerPrisonCeTour = false;
//        if (tourPrison == 4) {
//            tourPrison = 0;
//            montant = montant - 50;
//            enPrison = false;
//            System.out.println(nom + " est libere de prison.");
//        }
//    }

	public void lancer() {  // le joueur lance les 2 et recupere un tableau de valeur
        coupleDes.roulerTous();
    }
    public void deplacer() {
        int nombreDeplacement=coupleDes.faitLaSomme();
        int x=indexCaseCourante;
        indexCaseCourante+=nombreDeplacement;
        if (indexCaseCourante>=plateau.getCasesCount()) {  // si il passe par le depart
        	indexCaseCourante-=plateau.getCasesCount();
        	if(indexCaseCourante==0)indexCaseCourante=1;
            montant += 200;
            tour++;
        }
        deplacerVersindex(indexCaseCourante);
	    position.appliquerRegles(this);
    }

//	private void faireFailliteACauseLoyer() {
//		System.out.print(getNom()
//				+ " ne peut pas payer le loyer en entier. Il fait faillite.");
//	}


    public int compareTo(Object other) {    // sert au classement fianl des joueurs
        int nombre1 = ((Joueur) other).getMontant();
        int nombre2 = this.getMontant();
        if (nombre1 > nombre2) return -1;
        else if (nombre1 == nombre2) return 0;
        else return 1;
    }


    public void afficherLesProprietes(){ // affiche les proprietes
        int compteur=0; // sert pour la mise en forme avec les virgules
        System.out.print(getNom() +" est proprietaire de :") ;
    for (Constructible c : casespossedes) {
        if (compteur==0) {
        System.out.print(" " + c.getNom()) ;
    }
      else {
            System.out.print(", " + c.getNom()) ;
        }
        compteur++;
    }
        System.out.println(".") ;
    }


}

