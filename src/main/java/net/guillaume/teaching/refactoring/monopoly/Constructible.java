package net.guillaume.teaching.refactoring.monopoly;

public class Constructible extends Propriete {

	private final Couleur couleur;

	public Constructible(String nom, int coutAchat, int loyer, Couleur couleur) {
		super(nom, coutAchat, loyer);
		this.couleur = couleur;
	}

	public Couleur getCouleur() {
		return couleur;
	}

	
	@Override
	public int calculerLoyer() { // if(isLibre)
		Joueur proprietaire = getProprietaire();
		return proprietaire.aTousProprietesMemeCouleur(couleur)?getLoyerInitial() * 2 : getLoyerInitial();
	}

	@Override
	public void augmenterProprietes(Joueur joueurPayeur) {
		joueurPayeur.augmenterProprietaiesCouleur(couleur);
	}
}
