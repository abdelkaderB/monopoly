package net.guillaume.teaching.refactoring.monopoly;

public class CoupleDes {
	private De premier;
	private De deuxieme;
	
	public CoupleDes() {
		this.premier = new De();
		this.deuxieme = new De();
	}
	public void roulerTous(){
		premier.rouler();
		deuxieme.rouler();
	}
    protected boolean estUnDouble() {     // test si c'est un double
        return (premier.getValeur()==deuxieme.getValeur());
    }

    public int faitLaSomme() {    // calcul le total du lancer
        return (premier.getValeur()+deuxieme.getValeur());
    }
}



