package net.guillaume.teaching.refactoring.monopoly;


import java.util.LinkedHashMap;


public class Plateau {
	public static final int DEPART=1;
	public static final int PRISON=9;
	private static final Plateau INSTANCE=new Plateau();
	private LinkedHashMap<Integer,Case> cases = new LinkedHashMap<>();

	public static Plateau getInstance() {
		return INSTANCE;
	}
	
	private Plateau() {
		cases.put(1,new Speciale("Depart"));
		cases.put(2,new Constructible("Boulevard de Bellevile", 20, 2,Couleur.MARRON));
		cases.put(3,new Constructible("Rue Lecourbe", 40, 4, Couleur.MARRON));
		cases.put(4,new Speciale("Impot sur le Revenu"));
		cases.put(5,new Gare("Gare Mont-Parnasse", 120, 25));
		cases.put(6,new Constructible("Rue Vaugirard", 60, 6, Couleur.BLEU));
		cases.put(7,new Constructible("Rue De Courcelles", 60, 6, Couleur.BLEU));
		cases.put(8,new Constructible("Avenue de la Republique", 80, 8,Couleur.BLEU));
		cases.put(9,new Speciale("Prison"));
		cases.put(10,new Constructible("Avenue de Neuilly", 100, 10,Couleur.VIOLET));
		cases.put(11,new Constructible("Boulevard de la Villette", 100, 10,Couleur.VIOLET));
		cases.put(12,new Constructible("Rue de Paradis", 120, 12, Couleur.VIOLET));
		cases.put(13,new Gare("Gare De Lyon", 120, 25));
		cases.put(14,new Constructible("Avenue Mozart", 140, 14, Couleur.ORANGE));
		cases.put(15,new Constructible("Boulevard St Michel", 140, 14,Couleur.ORANGE));
		cases.put(16,new Constructible("Place Pigalle", 160, 16, Couleur.ORANGE));
		cases.put(17,new Constructible("Boulevard Malesherbes", 180, 18,Couleur.ROUGE));
		cases.put(18,new Constructible("Avenue Matignon", 180, 18, Couleur.ROUGE));
		cases.put(19,new Constructible("Avenue Henri Martin", 200, 20,Couleur.ROUGE));
		cases.put(20,new Gare("Gare Du Nord", 120, 25));
		cases.put(21,new Constructible("Place de la Bourse", 220, 22, Couleur.JAUNE));
		cases.put(22,new Constructible("Faubourg St Honore", 220, 20,Couleur.JAUNE));
		cases.put(23,new Constructible("Rue de la Fayette", 240, 24, Couleur.JAUNE));
		cases.put(24,new Speciale("Aller en Prison"));
		cases.put(25,new Constructible("Avenue Foch", 260, 26, Couleur.VERT));
		cases.put(26,new Constructible("Avenue de Breteuil", 260, 26,Couleur.VERT));
		cases.put(27,new Constructible("Boulevard des Capucines", 280, 28,Couleur.VERT));
		cases.put(28,new Gare("Gare Saint-Lazar", 120, 25));
		cases.put(29,new Constructible("Avenue des Champs Elysees", 300, 30,Couleur.BLEUMARINE));
		cases.put(30,new Speciale("Taxe de Luxe"));
		cases.put(31,new Constructible("Rue de la paix", 350, 35, Couleur.BLEUMARINE));
	}
	
	public Case getAt(int index) {
		return cases.get(index);
	}
	
	public int getCasesCount() {
		return cases.size();
	}
		
	
}
